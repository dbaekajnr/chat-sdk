//
// Copyright (c) 2018 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

//-------------------------------------------------------------------------------------------------------------------------------------------------
@objc protocol LoginEmailDelegate: class {

	func didLoginEmail()
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
class LoginEmailView: UIViewController, UITextFieldDelegate {

	@IBOutlet weak var delegate: LoginEmailDelegate?

	@IBOutlet var fieldEmail: UITextField!
	@IBOutlet var fieldPassword: UITextField!

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {

		super.viewDidLoad()

		let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(gestureRecognizer)
		gestureRecognizer.cancelsTouchesInView = false
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func viewWillDisappear(_ animated: Bool) {

		super.viewWillDisappear(animated)

		dismissKeyboard()
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	@objc func dismissKeyboard() {

		view.endEditing(true)
	}

	// MARK: - User actions
	//---------------------------------------------------------------------------------------------------------------------------------------------
	@IBAction func actionLogin(_ sender: Any) {

		let email = (fieldEmail.text ?? "").lowercased()
		let password = fieldPassword.text ?? ""

		if (email.count == 0)		{ ProgressHUD.showError("Please enter your email.");	return 	}
		if (password.count == 0)	{ ProgressHUD.showError("Please enter your password.");	return 	}

		ProgressHUD.show(nil, interaction: false)

        FUser.signIn(email: email, password: password) { user, error in
        
//        let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJmaXJlYmFzZS1hZG1pbnNkay10MDU3eUByb29rcGx1cy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsInN1YiI6ImZpcmViYXNlLWFkbWluc2RrLXQwNTd5QHJvb2twbHVzLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiaHR0cHM6XC9cL2lkZW50aXR5dG9vbGtpdC5nb29nbGVhcGlzLmNvbVwvZ29vZ2xlLmlkZW50aXR5LmlkZW50aXR5dG9vbGtpdC52MS5JZGVudGl0eVRvb2xraXQiLCJpYXQiOjE1NjY2MzM4NTYsImV4cCI6MTU2NjYzNzQ1NiwidWlkIjoiNDIyNDI0c2ZzZCIsImRhdGEiOnsiYXBpZCI6Ijc0MiIsImVtYWlsIjoia29maXJvb2tAbXlyb29rZXJ5LmNvbSJ9fQ.Wx6XoxcMwv1dA3ZW8M9iDRC9o1SAqeX4FbdV8kQHKgMIhVvC4XlH5Z_x1aUwKf02ICvRNYCDtr1EwKQG8xbYHGjYTtzAiulFWcTA6ErrMxc5a8zgOlIkCgdVrW9lYw_E-PNWMP9_PKPkNUFuuBvytv0jbbYd8NwIa09THVfFlqQ_xSbWD0i0OKZNB2TvIGy2_3H3cx-QG6IaUCnPGU0Pq_NIEzR1XYLkL5aJBY4dpC1qgWbWvOoTXEZJhl_mo6WDFHbuEBZy3zutbpL3DmUUKxW_wq3HZgmvsd6h1tpe18yvwL2q0dlLboPoerevD1clhOPqTW7WJs5mkpkBZcWsVg"
//        FUser.signIn(token: token) { user, error in
			if (error == nil) {
				self.dismiss(animated: true) {
					self.delegate?.didLoginEmail()
				}
			} else {
				ProgressHUD.showError(error!.localizedDescription)
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	@IBAction func actionDismiss(_ sender: Any) {

		dismiss(animated: true)
	}

	// MARK: - UITextField delegate
	//---------------------------------------------------------------------------------------------------------------------------------------------
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {

		if (textField == fieldEmail) {
			fieldPassword.becomeFirstResponder()
		}
		if (textField == fieldPassword) {
			actionLogin(0)
		}
		return true
	}
}
