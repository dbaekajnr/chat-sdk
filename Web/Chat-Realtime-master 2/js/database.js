let db;
let dbchat;
let dbmessage;

rookdb.connect().then((dbInstance) => {
    db = dbInstance;
    dbchat = db.getSchema().getDBChat();
    dbmessage = db.getSchema().getDBMessage();
});

const updateDBChat = (chat) => {
    // User is signed in.
    let userId = currentUser;
    var temp = chat;
    if (temp[FCHAT_MEMBERS].includes(userId)) {
        temp[FCHAT_LINKEDS] = chat[FCHAT_LINKEDS];
        temp[FCHAT_MEMBERS] = chat[FCHAT_MEMBERS];

        let senderId = chat[FCHAT_SENDERID];
        let groupId = chat[FCHAT_GROUPID];

        if (groupId.length === 0) {
            if (senderId !== userId) {
                let tempRecipientID = chat[FCHAT_RECIPIENTID];
                let tempRecipientFN = chat[FCHAT_RECIPIENTFULLNAME];
                let tempRecipientIni = chat[FCHAT_RECIPIENTINITIALS];
                let tempRecipientPic = chat[FCHAT_RECIPIENTPICTUREAT];

                temp[FCHAT_RECIPIENTID] = chat[FCHAT_SENDERID];
                temp[FCHAT_RECIPIENTFULLNAME] = chat[FCHAT_SENDERFULLNAME];
                temp[FCHAT_RECIPIENTINITIALS] = chat[FCHAT_SENDERINITIALS];
                temp[FCHAT_RECIPIENTPICTUREAT] = chat[FCHAT_SENDERPICTUREAT];

                temp[FCHAT_SENDERID] = tempRecipientID;
                temp[FCHAT_SENDERFULLNAME] = tempRecipientFN;
                temp[FCHAT_SENDERINITIALS] = tempRecipientIni;
                temp[FCHAT_SENDERPICTUREAT] = tempRecipientPic;
            }
        }
        let chatId = chat[FCHAT_CHATID];
        let text = chat[FCHAT_LASTMESSAGETEXT];
        temp[FCHAT_LASTMESSAGETEXT] = decrypt(text, chatId);
        //----------------------------------------------------------------------------------
        temp[FCHAT_TYPINGS] = chat[FCHAT_TYPINGS];
        temp[FCHAT_LASTREADS] = chat[FCHAT_LASTREADS];
        temp[FCHAT_MUTEDUNTILS] = chat[FCHAT_MUTEDUNTILS];
        temp["lastRead"] = 0;
        temp["mutedUntil"] = 0;
        temp["counter"] = 0;
        //----------------------------------------------------------------------------------
        let lastReads = chat[FCHAT_LASTREADS];
        let mutedUntils = chat[FCHAT_MUTEDUNTILS];
        if (lastReads)
            temp["lastRead"] = (lastReads[userId] != null) ? lastReads[userId] : 0;
        if (mutedUntils)
            temp["mutedUntil"] = (mutedUntils[userId] != null) ? mutedUntils[userId] : 0;
        //----------------------------------------------------------------------------------
        if (groupId.length === 0) {
            temp["details"] = temp[FCHAT_RECIPIENTFULLNAME]
        }
        if (groupId.length !== 0) {
            temp["details"] = chat[FCHAT_GROUPNAME]
        }
        //----------------------------------------------------------------------------------
        if (temp["lastRead"]) {
            let lastMessageDate = chat[FCHAT_LASTMESSAGEDATE];
            if (temp["lastRead"] >= lastMessageDate)
                temp["counter"] = 0;
        }
        //----------------------------------------------------------------------------------
        let archiveds = chat[FCHAT_ARCHIVEDS];
        let deleteds = chat[FCHAT_DELETEDS];
        if (archiveds)
            temp["isArchived"] = archiveds[userId];
        if (deleteds)
            temp["isDeleted"] = deleteds[userId];
        let row = [dbchat.createRow(temp)];
        return db.insertOrReplace().into(dbchat).values(row).exec();
    }
};

const updateDBMessage = (message) => {
    // User is signed in.
    let userId = currentUser;
    var temp = message;
    if (temp[FMESSAGE_MEMBERS].includes(userId)) {
        temp[FMESSAGE_MEMBERS] = message[FCHAT_MEMBERS];
        let chatId = message[FMESSAGE_CHATID];
        let text = message[FMESSAGE_TEXT];
        temp[FMESSAGE_TEXT] = decrypt(text, chatId);
        let row = [dbmessage.createRow(temp)];
        return db.insertOrReplace().into(dbmessage).values(row).exec();
    }
};

const updateDBCounter = (message) => {
    let userId = currentUser;
    let chatId = message[FMESSAGE_CHATID];
    let senderId = message[FMESSAGE_SENDERID];
    let createdAt = message[FMESSAGE_CREATEDAT];

    if (senderId !== userId) {
        let pred = dbmessage.chatId.eq(chatId);
        db.select().from(dbchat).where(pred).exec().then((results) => {
            if (results[0]) {
                let dbchat = results[0];
                if (createdAt > dbchat.lastRead) {
                    updateCounter();
                    //TODO: Refresh chats screen
                }
            } else {
                initCounter(chatId);
            }
        });
    }
};

const messageLastUpdateAt = () => {
    return db.select(dbmessage.updatedAt).from(dbmessage).orderBy(dbmessage.updatedAt, lf.Order.ASC).exec();
};

const updateCounter = () => {
    dbchat.counter += 1;
    // return db.insertOrReplace().into(dbchat).values(row).exec();
};

const initCounter = (chatId) => {

};