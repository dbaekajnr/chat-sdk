(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();
const ajax = (method, send, callback) => {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            callback(this.responseText);
        }
    };
    xmlhttp.open(method, apis, true);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    if (send) {
        xmlhttp.send(send);
    } else {
        xmlhttp.send();
    }
};

const postNotification = (notificationName, elem, data = null) => {
    var event = new CustomEvent(notificationName, {detail: data});
    elem.dispatchEvent(event);
};

// Cached User Login
/*
When app launches get the session apid and then call for company uid to use for chat.
 */
let userlogin;

ajax("POST", "data=cek", res => {
    const a = JSON.parse(res);
    if (a.status == 'success') {
        document.getElementsByClassName('app-one')[0].style.display = "block";
        document.getElementById("login").style.display = "none";
        document.getElementsByClassName('me')[0].src = a.avatar;
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                // User is signed in.
                let userId = auth.currentUser.uid;
                userRef.child(userId).once('value').then(function (snapshot) {
                    let values = snapshot.val();
                    if (values) {
                        values["updatedAt"] = firebase.database.ServerValue.TIMESTAMP;
                        values["lastTerminate"] = firebase.database.ServerValue.TIMESTAMP;
                        values["lastActive"] = firebase.database.ServerValue.TIMESTAMP;
                        userRef.child("test1").update(values);
                    }
                });
            } else {
                console.log('No user is signed in.');
            }
        });
        userlogin = a.user;
        document.getElementById("heading-name-meta").innerHTML = "Public";
        document.getElementById("heading-online").innerHTML = "rooms";
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                currentUser = auth.currentUser.uid;
                chat_realtime(userRef, messageRef, apis, a.user, a.avatar, imageDir, domain)
            }
        });
    } else {
        document.getElementsByClassName('app-one')[0].style.display = "none";
        document.getElementById("login").style.display = "block";
    }
});

// New User Login
document.getElementsByClassName("form-signin")[0].onsubmit = () => {
    loginFunction()
};

function loginFunction() {
    document.querySelector("#ref").innerHTML = "<center>Wait...</center>";
    const uname = document.querySelector("#username").value;
    const avatar = document.querySelector("#avatar").value;

    if (uname && avatar) {
        ajax("POST", `data=login&name=${uname}&avatar=${avatar}`, res => {
            const response = JSON.parse(res);
            if (response.status === 'success') {
                firebase.auth().onAuthStateChanged(function (user) {
                    if (user) {
                        // User is signed in.
                        let userId = auth.currentUser.uid;
                        userRef.child(userId).once('value').then(function (snapshot) {
                            let values = snapshot.val();
                            if (values) {
                                values["updatedAt"] = firebase.database.ServerValue.TIMESTAMP;
                                values["lastTerminate"] = firebase.database.ServerValue.TIMESTAMP;
                                values["lastActive"] = firebase.database.ServerValue.TIMESTAMP;
                                userRef.child("test1").update(values);
                            }
                        });
                    } else {
                        console.log('No user is signed in.');
                    }
                });
                window.location.href = domain;
            } else {
                document.getElementById("ref").innerHTML = "<div class='alert alert-danger'>Username sudah di pakai.</div>";
            }
        });
    } else {
        alert('Form input ada yang belum di isi')
    }
}

// user logout
document.getElementsByClassName("heading-logout")[0].addEventListener("click", () => {
    ajax("POST", "data=logout", res => {
        const a = JSON.parse(res);
        if (a.status == 'success') {
            const b = {
                name: userlogin,
                tipe: 'logout'
            };
            userRef.push(b);
            setTimeout(() => {
                window.location.href = domain;
            }, 1500);
        }
    });
});

//Styles for the window
document.getElementsByClassName("heading-compose")[0].addEventListener("click", () => {
    document.getElementsByClassName('side-two')[0].style.left = "0";
});

document.getElementsByClassName("newMessage-back")[0].addEventListener("click", () => {
    document.getElementsByClassName('side-two')[0].style.left = "-100%";
});

document.getElementsByClassName("user-back")[0].addEventListener("click", () => {
    document.getElementsByClassName('side')[0].style.display = "block";
});

//App itself in operation
//(userRef, messageRef, apis, a.user, a.avatar, imageDir, domain)
var chat_realtime = (userRef, messageRef, apis, m, n, imageDir, domain) => {
    let recipientId = "";
    let allUser;
    let chatUser;
    let messages = [];
    let no = 0;
    const limit = 10;
    let uKe = 'Public';
    let uTipe = 'rooms';
    let tampungImg = [];

    // Contains the number for updated unread messages
    let inbox = 0;
    if (inbox == 0) {
        $(".inbox-status").hide();
    }

    //TODO:
    //Function to post to user table in mysql
    const userMysql = (callback) => {
        $.ajax({
            url: apis,
            type: "post",
            data: 'data=user',
            crossDomain: true,
            dataType: 'json',
            success(a) {
                callback(a);
            }
        })
    };

    const sidebar = document.querySelector('.side-two .compose-sideBar');
    const messageBox = document.querySelector('#conversation .messages');

    //Function to post to chat table in mysql
    const chatMysql = (f, e, callback) => {
        $.ajax({
            url: apis,
            type: "post",
            data: {
                data: 'message',
                ke: f,
                tipe: e
            },
            crossDomain: true,
            dataType: 'json',
            success(a) {
                callback(a);
            }
        })
    };

    //TODO:
    // userMysql(({all, chat}) => {
    //     allUser = all;
    //     chatUser = chat;
    //     //Users list on sidebar
    //     allUser.forEach(a => {
    //         if (a.name != m) {
    //             sideTwoHTML(a);
    //         }
    //     });
    //     //Chats list here
    //     if (chatUser.length > 0) {
    //         chatUser.forEach(a => {
    //             // sideOneHTML(a);
    //         });
    //     }
    //
    //
    //     chatMysql('rooms', 'Public', a => {
    //         messages = a;
    //         no = 0;
    //       //  document.getElementsByClassName('messages')[0].innerHTML = "";
    //         if (messages.length <= limit) {
    //             $('#message-previous').hide();
    //         } else {
    //             $('#message-previous').show();
    //         }
    //         let opsid = 0;
    //         messages.forEach(a => {
    //             if (opsid < limit) {
    //               //  messageHTML(messages[no]);
    //                 no++;
    //             }
    //             opsid++;
    //         });
    //         $('.placeholder').magnificPopup({
    //             type: 'image',
    //             closeOnContentClick: true,
    //             mainClass: 'mfp-img-mobile',
    //             image: {
    //                 verticalFit: true
    //             }
    //
    //         });
    //         scrollBottom();
    //     });
    // });


    const chatsInit = () => {
        let child = FCHAT_LINKEDS + "/" + currentUser;
        let query = chatRef.orderByChild(child).equalTo(true);

        query.on("child_added", snapshot => {
            if (snapshot.exists()) {
                let chat = snapshot.val();
                if (chat[FCHAT_CREATEDAT] != null) {
                    updateDBChat(chat);
                    postNotification(POST_SIDEBAR_REFRESH, sidebar);
                }
            }
        });
        query.on("child_changed", snapshot => {
            if (snapshot.exists()) {
                let chat = snapshot.val();
                if (chat[FCHAT_CREATEDAT] != null) {
                    updateDBChat(chat);
                    postNotification(POST_SIDEBAR_REFRESH, sidebar);
                }
            }
        })
    };

    const messagesInit = () => {
        let userId = currentUser;
        messageLastUpdateAt().then((result) => {
            let lastUpdatedAt = (result.length) ? result[result.length - 1]["updatedAt"] : 0;
            let messageHead = messageRef.child(userId);
            let query = messageHead.orderByChild(FMESSAGE_UPDATEDAT).startAt(lastUpdatedAt + 1);

            query.on("child_added", snapshot => {
                if (snapshot.exists()) {
                    let message = snapshot.val();
                    if (message[FMESSAGE_CREATEDAT] != null) {
                        updateDBMessage(message);
                        updateDBCounter(message);
                        postNotification(POST_MESSAGE_REFRESH, messageBox);
                    }
                }
            });
            query.on("child_changed", snapshot => {
                if (snapshot.exists()) {
                    let message = snapshot.val();
                    if (message[FMESSAGE_CREATEDAT] != null) {
                        updateDBMessage(message);
                        postNotification(POST_MESSAGE_REFRESH, messageBox);  //TODO: account for typing and last read
                    }
                }
            })
        });
    };


    chatsInit();
    messagesInit();

    sidebar.addEventListener(POST_SIDEBAR_REFRESH, () => {
        let pred = lf.op.and(dbchat.isDeleted.eq(false), dbchat.isArchived.eq(false));
        db.select().from(dbchat).where(pred).orderBy(dbchat.lastMessageDate, lf.Order.DESC).exec().then((results) => {
            for (let chat of results)
                sideOneHTML(chat);
        });
    });

    messageBox.addEventListener(POST_MESSAGE_REFRESH, () => {
        if (recipientId.length) {
            messageBox.innerHTML = "";
            let chatId = recipientId; //getChatId(recipientId);
            let pred = lf.op.and(dbmessage.isDeleted.eq(false), dbmessage.chatId.eq(chatId));
            db.select().from(dbmessage).where(pred).orderBy(dbchat.createdAt, lf.Order.ASC).exec().then((results) => {
                if (results.length > limit) {
                    $(".message-previous").show();
                } else {
                    $(".message-previous").hide();
                }
                let message = results[0];
                let name = (message.groupId.length) ? message.groupName : message.senderFullname ;
                headingHTML("", name, 'online');
                for (let message of results) {
                    messageHTML(message, true);
                }
            });
        }
    });

    const getChatId = (recipientId) => {
        let members = [currentUser, recipientId];
        let sorted = members.sort(caseInsensitiveSort);
        let joined = sorted.join('');
        return md5(joined);
    };

    const getGroupId = (groupId) => {

    };

    //General when you click on chat on the left pane
    $('body').on('click', '.side-one .sideBar-body', function () {
        const a = $(this).attr('id');
        recipientId = a;
        postNotification(POST_MESSAGE_REFRESH, messageBox);
        const tipe = $(this).data('tipe');
        const av = $(this).data('avatar');
        const st = $(this).data('status');
        $('.side-one .sideBar-body').removeClass("active");
        $(this).addClass("active");
        $(`.side-one #${a} .inbox-count`).remove();
        uKe = a;
        uTipe = tipe;
        //   headingHTML(av, a, st);
        chatMysql(tipe, a, a => {
            messages = a;
            no = 0;
            //document.getElementsByClassName('messages')[0].innerHTML = "";
            // if (messages.length > limit) {
            //     $(".message-previous").show();
            // } else {
            //     $(".message-previous").hide();
            // }
            let opsid = 0;
            messages.forEach(a => {
                if (opsid < limit) {
                    //  messageHTML(messages[no]);
                    no++;
                }
                opsid++;
            });

            $('.placeholder').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-img-mobile',
                image: {
                    verticalFit: true
                }

            });
            scrollBottom();
        });
        const $window = $(window);

        function checkWidth() {
            const windowsize = $window.width();
            if (windowsize <= 700) {
                $(".side").css({
                    "display": "none"
                });
            }
        }

        checkWidth();
        $(window).resize(checkWidth);
        return false
    });

    //Paste users on side two of list of users
    userRef.on("value", a => {
        //console.log("added", a.key, a.val());
        if (a.val().tipe == 'login') {
            if (a.val().name != m) {
                if ($(`#${a.val().name}`).length) {
                    $(`#${a.val().name} .contact-status`).removeClass('off');
                    $(`#${a.val().name} .contact-status`).addClass('on');
                    $(`#${a.val().name} .time-meta`).html(timeToWords(a.val().login))
                } else {
                    const newUser = {
                        status: "online",
                        name: a.val().name,
                        login: a.val().login,
                        avatar: a.val().avatar
                    };
                    allUser.push(newUser);
                    sideTwoHTML(newUser);
                }
            }
        } else {
            $(`#${a.val().name} .contact-status`).removeClass('on');
            $(`#${a.val().name} .contact-status`).addClass('off');
        }
    });


    // firebase.auth().onAuthStateChanged(function (user) {
    //     if (user) {
    //         // User is signed in.
    //         let userId = auth.currentUser.uid;
    //         messageRef.on("value", snapshot => {
    //             let chats = [];
    //             snapshot.forEach(chat => {
    //                 chat = chat.val();
    //                 var temp = chat;
    //                 if (temp[FCHAT_MEMBERS].includes(userId)) {
    //                     temp[FCHAT_LINKEDS] = chat[FCHAT_LINKEDS];
    //                     temp[FCHAT_MEMBERS] = chat[FCHAT_MEMBERS];
    //
    //                     let senderId = chat[FCHAT_SENDERID];
    //                     let groupId = chat[FCHAT_GROUPID];
    //
    //                     if (groupId.length === 0) {
    //                         if (senderId !== userId) {
    //                             let tempRecipientID = chat[FCHAT_RECIPIENTID];
    //                             let tempRecipientFN = chat[FCHAT_RECIPIENTFULLNAME];
    //                             let tempRecipientIni = chat[FCHAT_RECIPIENTINITIALS];
    //                             let tempRecipientPic = chat[FCHAT_RECIPIENTPICTUREAT];
    //
    //                             temp[FCHAT_RECIPIENTID] = chat[FCHAT_SENDERID];
    //                             temp[FCHAT_RECIPIENTFULLNAME] = chat[FCHAT_SENDERFULLNAME];
    //                             temp[FCHAT_RECIPIENTINITIALS] = chat[FCHAT_SENDERINITIALS];
    //                             temp[FCHAT_RECIPIENTPICTUREAT] = chat[FCHAT_SENDERPICTUREAT];
    //
    //                             temp[FCHAT_SENDERID] = tempRecipientID;
    //                             temp[FCHAT_SENDERFULLNAME] = tempRecipientFN;
    //                             temp[FCHAT_SENDERINITIALS] = tempRecipientIni;
    //                             temp[FCHAT_SENDERPICTUREAT] = tempRecipientPic;
    //                         }
    //                     }
    //                     let chatId = chat[FCHAT_CHATID];
    //                     let text = chat[FCHAT_LASTMESSAGETEXT];
    //                     temp[FCHAT_LASTMESSAGETEXT] = decrypt(text, chatId);
    //                     //----------------------------------------------------------------------------------
    //                     temp[FCHAT_TYPINGS] = chat[FCHAT_TYPINGS];
    //                     temp[FCHAT_LASTREADS] = chat[FCHAT_LASTREADS];
    //                     temp[FCHAT_MUTEDUNTILS] = chat[FCHAT_MUTEDUNTILS];
    //                     //----------------------------------------------------------------------------------
    //                     let lastReads = chat[FCHAT_LASTREADS];
    //                     let mutedUntils = chat[FCHAT_MUTEDUNTILS];
    //                     if (lastReads)
    //                         temp["lastRead"] = lastReads[userId];
    //                     if (mutedUntils)
    //                         temp["mutedUntil"] = mutedUntils[userId];
    //                     //----------------------------------------------------------------------------------
    //                     if (groupId.length === 0) {
    //                         temp["details"] = temp[FCHAT_RECIPIENTFULLNAME]
    //                     }
    //                     if (groupId.length !== 0) {
    //                         temp["details"] = chat[FCHAT_GROUPNAME]
    //                     }
    //                     //----------------------------------------------------------------------------------
    //                     if (temp["lastRead"]) {
    //                         let lastMessageDate = chat[FCHAT_LASTMESSAGEDATE];
    //                         if (temp["lastRead"] >= lastMessageDate)
    //                             temp["counter"] = 0;
    //                     }
    //                     //----------------------------------------------------------------------------------
    //                     let archiveds = chat[FCHAT_ARCHIVEDS];
    //                     let deleteds = chat[FCHAT_DELETEDS];
    //                     if (archiveds)
    //                         temp["isArchived"] = archiveds[userId];
    //                     if (deleteds)
    //                         temp["isDeleted"] = deleteds[userId];
    //                     chats.push(temp);
    //                 }
    //             });
    //             chats.sort(descCompare);
    //             console.log(chats);
    //             for (let chat of chats)
    //                 sideOneHTML(chat);
    //             //TODO do the rendering here using dictionary of chats
    //         });
    //     } else {
    //         console.log('No user is signed in.');
    //     }
    // });


    //New message came through and add to correct place
    // messageRef.on("child_added", a => {
    //     //console.log("added", a.key, a.val());
    //     const b = a.val().name;
    //
    //     const ke = a.val().ke;
    //
    //     // inbox rooms - use for groups
    //     if ($(`#${ke}`).data('tipe') == 'rooms') {
    //         if (uKe == ke) {
    //             messageHTML(a.val(), true);
    //         } else {
    //             inbox++;
    //             //message count
    //             document.getElementsByClassName("inbox-status")[0].innerHTML = inbox;
    //             $(".inbox-status").show();
    //         }
    //     }
    //
    //     // inbox user
    //     else {
    //         // inbox user
    //         if (ke == m) {
    //             if (!$(`.side-one #${b}`).length) {
    //                 const newUser = {
    //                     status: "online",
    //                     name: b,
    //                     date: a.val().date,
    //                     avatar: a.val().avatar,
    //                     selektor: "from"
    //                 };
    //                 chatUser.push(newUser);
    //                 // sideOneHTML(newUser);
    //             }
    //             $(`.side-one #${b} .time-meta`).html(timeToWords(a.val().date));
    //             if (uKe == b) {
    //                 messageHTML(a.val(), true);
    //                 $(`.side-one #${b} .sideBar-message`).html(htmlEntities(a.val().message));
    //             } else {
    //                 let co = 1;
    //                 if ($(`.side-one #${b} .inbox-count`).length) {
    //                     co = parseInt($(`.side-one #${b} .inbox-count`).html()) + 1;
    //                 }
    //                 $(`.side-one #${b} .sideBar-message`).html(`${htmlEntities(a.val().message)} <span class="inbox-count pull-right">${co}</span>`);
    //             }
    //         }
    //
    //         // send message
    //         else if (b == m) {
    //             $(`.side-one #${ke} .time-meta`).html(timeToWords(a.val().date));
    //             $(`.side-one #${ke} .sideBar-message`).html(`<i class="fa fa-check"></i> ${htmlEntities(a.val().message)}`);
    //             if (uKe == ke) {
    //                 messageHTML(a.val(), true);
    //             }
    //         }
    //     }
    //     //For displaying images
    //     $('.placeholder').magnificPopup({
    //         type: 'image',
    //         closeOnContentClick: true,
    //         mainClass: 'mfp-img-mobile',
    //         image: {
    //             verticalFit: true
    //         }
    //
    //     });
    //     if (uKe == ke) {
    //         scrollBottom();
    //     }
    // });

    //Set the name at the top. Not needed for Rook
    //TODO: redundant
    function headingHTML(avatar, name, status) {
        document.getElementsByClassName('you')[0].src = avatar;
        document.getElementById('heading-name-meta').innerHTML = name;
        document.getElementById('heading-online').innerHTML = status;
    }

    //Add message to conversation function
    function messageHTML(a, bottom) {
        if ($("#" + a.objectId).length) {
            //Chat exists so run check and mark old for remove
            // let id = $("#" + chats.objectId).prop("classList")[2];
            // if (a.objectId != recipientId) {
            //     $("#" + a.objectId).detach();
            //     return;
            // } else
            return;
        }
        let incoming = (a.senderId != currentUser);
        const image = (a.image != undefined ? a.image : a.images);
        let b = "";
        //Sender
        if (!incoming) {
            b += `<div id="${a.objectId}" class="row message-body">`;
            b += '  <div class="col-sm-12 message-main-sender">';
            b += '	<div class="sender">';
            b += `      <div class="message-text">${image != '' ? `<a title="Zoom" href="${imageDir}/${image}" class="placeholder"><img class="imageDir" src="${imageDir}/${image}"/></a>` : ''}${urltag(htmlEntities(a.text))}</div>`;
            b += `      <span class="message-time pull-right">${timeToWords(a.updatedAt)}</span>`;
            b += '	</div>';
            b += '  </div>';
            b += '</div>';
        }
        //Receiver
        else {
            b += `<div id="${a.objectId}" class="row message-body">`;
            b += '  <div class="col-sm-12 message-main-receiver">';
            b += '	<div class="receiver">';
            if (uKe == "Public") {
                const sub = a.name.substring(0, 1).toLowerCase();
                let col = "EC9E74";
                const r_a = ["a", "k", "7"];
                const r_b = ["b", "m", "1", "f"];
                const r_c = ["c", "w", "3"];
                const r_d = ["d", "s", "9"];
                const r_e = ["e", "i", "0"];
                const r_f = ["t", "h", "6"];
                const r_g = ["g", "u", "2"];
                const r_h = ["p", "z", "8", "l"];
                const r_i = ["o", "x", "5"];
                const r_j = ["q", "y", "4"];
                const r_k = ["j", "v", "r"];
                if (r_a.includes(sub)) {
                    col = "dfb610";
                } else if (r_b.includes(sub)) {
                    col = "8b7add";
                } else if (r_c.includes(sub)) {
                    col = "91ab01";
                } else if (r_d.includes(sub)) {
                    col = "6bcbef";
                } else if (r_e.includes(sub)) {
                    col = "fe7c7f";
                } else if (r_f.includes(sub)) {
                    col = "e542a3";
                } else if (r_g.includes(sub)) {
                    col = "b04632";
                } else if (r_h.includes(sub)) {
                    col = "ff8f2c";
                } else if (r_i.includes(sub)) {
                    col = "029d00";
                } else if (r_j.includes(sub)) {
                    col = "ba33dc";
                } else if (r_k.includes(sub)) {
                    col = "59d368";
                }
                b += `<a class="message-username" style="color:#${col} !important">${a.name}</a>`;
            }
            b += `      <div class="message-text">${image != '' ? `<a title="Zoom" href="${imageDir}/${image}" class="placeholder"><img class="imageDir" src="${imageDir}/${image}"/></a>` : ''}${urltag(htmlEntities(a.text))}</div>`;
            b += `      <span class="message-time pull-right">${timeToWords(a.updatedAt)}</span>`;
            b += '	</div>';
            b += '  </div>';
            b += '</div>';
        }
        if (bottom != undefined) {
            $('#conversation .messages').append(b);
        } else {
            $('#conversation .messages').prepend(b);
        }
    }

    //Load the chat side on left function
    function sideOneHTML(chats) {
        let shouldRemove = false;
        if ($("#" + chats.objectId).length) {
            //Chat exists so run check and mark old for remove
            let chat_time = $("#" + chats.objectId).prop("classList")[2];
            if (chat_time < chats.updatedAt)
                shouldRemove = true;
            else
                return;
        }
        let b = "";
        b += `<div id="${chats.objectId}" class="row sideBar-body ${chats.updatedAt}" data-tipe="users" data-login="${chats.lastMessageDate}" data-avatar="${chats.senderPictureAt}" data-status="online" id="${chats.details}">`;
        b += '	<div class="col-sm-3 col-xs-3 sideBar-avatar">';
        b += '  	<div class="avatar-icon">';
        b += `            <span class="contact-status ${chats.counter === 0 ? 'off' : 'on'}"></span>`;
        b += `            <img src="https://avatars3.githubusercontent.com/u/4948333">`;
        b += '  	</div>';
        b += '	</div>';
        b += '	<div class="col-sm-9 col-xs-9 sideBar-main">';
        b += '  	<div class="row">';
        b += '			<div class="col-sm-8 col-xs-8 sideBar-name">';
        b += `                <span class="name-meta">${chats.details}</span>`;
        b += '			</div>';
        b += '			<div class="col-sm-4 col-xs-4 pull-right sideBar-time">';
        b += `                <span class="time-meta pull-right">${timeToWords(chats.lastMessageDate)}</span>`;
        b += '			</div>';
        b += '			<div class="col-sm-12 sideBar-message">';
        b += htmlEntities(chats.lastMessageText);
        //  }
        // }
        b += '  		</div>';
        b += '  	</div>';
        b += '	</div>';
        b += '</div>';
        if (shouldRemove)
            $("#" + chats.objectId).detach();
        $('.side-one .sideBar').prepend(b);
    }

    //Load the users on the left function. Redundant for Rook
    //TODO redundant
    function sideTwoHTML({login, avatar, status, name}) {
        let b = "";
        b += `<div class="row sideBar-body" data-tipe="users" data-login="${login}" data-avatar="${avatar}" data-status="${status == 'online' ? 'online' : 'offline'}" id="${name}">`;
        b += '<div class="col-sm-3 col-xs-3 sideBar-avatar">';
        b += '  <div class="avatar-icon">';
        b += `    <span class="contact-status ${status == 'online' ? 'on' : 'off'}"></span>`;
        b += `    <img src="${avatar}">`;
        b += '  </div>';
        b += '</div>';
        b += '<div class="col-sm-9 col-xs-9 sideBar-main">';
        b += '  <div class="row">';
        b += '	<div class="col-sm-8 col-xs-8 sideBar-name">';
        b += `      <span class="name-meta">${name}</span>`;
        b += '	</div>';
        b += '	<div class="col-sm-4 col-xs-4 pull-right sideBar-time">';
        b += `      <span class="time-meta pull-right">${timeToWords(login)}</span>`;
        b += '	</div>';
        b += '  </div>';
        b += '</div>';
        b += '</div>';
        $('.side-two .compose-sideBar').prepend(b);
    }

    //Clean text message for greater than and less to prevent it from mistaken as html
    function htmlEntities(a) {
        return String(a).replace(/</g, '&lt;').replace(/>/g, '&gt;')
    }

    //Function useful for anticipating links and processing them such as videos and what not
    function urltag(d, e) {
        const f = {
            yutub: {
                regex: /(^|)(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*)(\s+|$)/ig,
                template: "<iframe class='yutub' src='//www.youtube.com/embed/$3' frameborder='0' allowfullscreen></iframe>"
            },
            link: {
                regex: /((^|)(https|http|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig,
                template: "<a href='$1' target='_BLANK'>$1</a>"
            },
            email: {
                regex: /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi,
                template: '<a href=\"mailto:$1\">$1</a>'
            }
        };
        const g = $.extend(f, e);
        $.each(g, (a, {regex, template}) => {
            d = d.replace(regex, template)
        });
        return d
    }

    // upload images
    //----------------------------
    function convertDataURIToBinary(dataURI) {
        const BASE64_MARKER = ';base64,';
        const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        const base64 = dataURI.substring(base64Index);
        const raw = window.atob(base64);
        const rawLength = raw.length;
        const array = new Uint8Array(new ArrayBuffer(rawLength));

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    }

    function readMultipleImg({target}) {
        if (!$('.imagetmp').is(':visible')) {
            $('.imagetmp').css("display", "block");
        }
        //Retrieve all the files from the FileList object
        const files = target.files;

        if (files) {
            for (let i = 0, f; f = files[i]; i++) {
                if (/(jpe?g|png|gif)$/i.test(f.type)) {
                    const r = new FileReader();
                    r.onload = (({type, name, size}) => ({target}) => {
                        const base64Img = target.result;
                        const binaryImg = convertDataURIToBinary(base64Img);
                        const blob = new Blob([binaryImg], {
                            type: type
                        });
                        const x = tampungImg.length;
                        const blobURL = window.URL.createObjectURL(blob);
                        const fileName = makeid(name.split('.').pop());
                        tampungImg[x] = {
                            name: fileName,
                            type: type,
                            size: size,
                            binary: Array.from(binaryImg)
                        };
                        $('#reviewImg').append(`<img src="${blobURL}" data-idx="${fileName}" class="tmpImg" title="Remove"/>`);
                    })(f);

                    r.readAsDataURL(f);
                } else {
                    alert("Failed file type");
                }
            }
        } else {
            alert("Failed to load files");
        }
    }

    function makeid(x) {
        const d = new Date();
        let text = d.getTime();
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return `${text}.${x}`;
    }

    //----------------------------------------

    //-----------------HELPER FUNCTIONS--------------------
    // Function to scroll to bottom of messages when new messagem
    function scrollBottom() {
        setTimeout(() => {
            const cc = $('#conversation');
            const dd = cc[0].scrollHeight;
            cc.animate({
                scrollTop: dd
            }, 500);
            $("body .message-scroll").hide();
            $("body .message-previous").hide();
        }, 1000);
    }

    //Function to scroll to top of messages
    function scrollTop() {
        setTimeout(() => {
            const cc = $('#conversation');
            cc.animate({
                scrollTop: 0
            }, 500);
        }, 1000);
    }

    //function to convert time into words
    const timeToWords = (timestamp) => {
        const since = timestamp, // Saturday, 08-Apr-17 21:00:00 UTC
            seconds = (new Date().getTime() - since) / 1000;

        let elapsed = "";
        if (seconds < 60) {
            elapsed = "Just now"
        } else if (seconds < 60 * 60) {
            let minutes = Math.floor(seconds / 60);
            let text = (minutes > 1) ? "mins" : "min";
            elapsed = `${minutes} ${text}`;
        } else if (seconds < 24 * 60 * 60) {
            let hours = Math.floor(seconds / (60 * 60));
            let text = (hours > 1) ? "hours" : "hour";
            elapsed = `${hours} ${text}`;
        } else if (seconds < 7 * 24 * 60 * 60) {
            let date = new Date(timestamp);
            let options = {weekday: 'long'};
            elapsed = date.toLocaleDateString('en-US', options);
        } else {
            let date = new Date(timestamp);
            let options = {year: 'numeric', month: 'numeric', day: 'numeric'};
            elapsed = date.toLocaleDateString('en-US', options);
        }
        return elapsed;
    };

    //----------------------------

    // emojiPicker
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '//onesignal.github.io/emoji-picker/lib/img/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();

    //Check size of window and resize it depending on the size of the screen
    const $window = $(window);

    function checkWidth() {
        const windowsize = $window.width();
        if (windowsize > 700) {
            document.getElementsByClassName("side")[0].style.display = "block";
            $(".user-back").hide();
            $(".sideBar-body").removeClass("user-body");
        } else if (windowsize <= 700) {
            $(".user-back").show();
            $(".sideBar-body").addClass("user-body");
        }
    }

    checkWidth();
    $(window).resize(checkWidth);


    //Get class name for user clicking on a chat user when mobile size
    $("body").on("click", ".user-body", () => {
        document.getElementsByClassName("side")[0].style.display = "none";
    });

    //send chat
    document.getElementById("send").addEventListener("click", () => {
        const a = new Date();
        const b = a.getDate();
        const c = (a.getMonth() + 1);
        const d = a.getFullYear();
        const e = a.getHours();
        const f = a.getMinutes();
        const g = a.getSeconds();
        const date = `${d}-${c < 10 ? `0${c}` : c}-${b < 10 ? `0${b}` : b} ${e < 10 ? `0${e}` : e}:${f < 10 ? `0${f}` : f}:${g < 10 ? `0${g}` : g}`;
        const il = tampungImg.length;
        if (document.getElementById('comment').value != '') {
            ajax("POST", `data=send&name=${m}&ke=${uKe}&avatar=${n}&message=${document.getElementById('comment').value}&images=${JSON.stringify(tampungImg)}&tipe=${uTipe}&date=${date}`, res => {

                const a = JSON.parse(res);

                // insert firebase
                if (il > 0) {
                    for (hit = 0; hit < il; hit++) {
                        if (hit == 0) {
                            var i = {
                                data: 'send',
                                name: m,
                                ke: uKe,
                                avatar: n,
                                message: document.getElementById('comment').value,
                                images: tampungImg[hit].name,
                                tipe: uTipe,
                                date
                            };
                        } else {
                            var i = {
                                data: 'send',
                                name: m,
                                ke: uKe,
                                avatar: n,
                                message: '',
                                images: tampungImg[hit].name,
                                tipe: uTipe,
                                date
                            };
                        }
                        messageRef.push(i);
                    }
                } else {
                    var i = {
                        data: 'send',
                        name: m,
                        ke: uKe,
                        avatar: n,
                        message: document.getElementById('comment').value,
                        images: '',
                        tipe: uTipe,
                        date
                    };

                    // push firebase
                    messageRef.push(i);
                }
                tampungImg = [];
                document.getElementById('comment').value = "";
                document.getElementsByClassName('emoji-wysiwyg-editor')[0].innerHTML = "";
                document.getElementById('reviewImg').innerHTML = "";
                document.getElementsByClassName('imagetmp')[0].style.display = "none";
                scrollBottom();

            });
        } else {
            alert('Please fill atlease message!')
        }
    });


    //General when you click on the people list on the left pane
    //TODO: Do not need side two. Might be redundant for us
    // $('body').on('click', '.side-two .sideBar-body', function () {
    //     messages = [];
    //     const a = $(this).attr('id');
    //     const tipe = $(this).data('tipe');
    //     const av = $(this).data('avatar');
    //     const st = $(this).data('status');
    //     const lg = $(this).data('login');
    //     uKe = a;
    //     uTipe = tipe;
    //     headingHTML(av, a, st);
    //     if ($(`.side-one #${a}`).length) {
    //         chatMysql(tipe, a, a => {
    //             messages = a;
    //             no = 0;
    //             document.getElementsByClassName('messages')[0].innerHTML = "";
    //             if (messages.length > limit) {
    //                 $(".message-previous").show();
    //             } else {
    //                 $(".message-previous").hide();
    //             }
    //             let opsid = 0;
    //             messages.forEach(a => {
    //                 if (opsid < limit) {
    //                    // messageHTML(messages[no]);
    //                     no++;
    //                 }
    //                 opsid++;
    //             });
    //         });
    //     } else {
    //         no = 0;
    //         document.getElementsByClassName('messages')[0].innerHTML = "";
    //         $(".message-previous").hide();
    //         const newUser = {
    //             status: st,
    //             name: a,
    //             date: lg,
    //             avatar: av
    //         };
    //         chatUser.push(newUser);
    //         //   sideOneHTML(newUser);
    //     }
    //     $('.side-one .sideBar-body').removeClass("active");
    //     $(`.side-one #${a}`).addClass("active");
    //
    //     $('.placeholder').magnificPopup({
    //         type: 'image',
    //         closeOnContentClick: true,
    //         mainClass: 'mfp-img-mobile',
    //         image: {
    //             verticalFit: true
    //         }
    //
    //     });
    //     scrollBottom();
    //     document.getElementsByClassName('side-two')[0].style.left = "-100%";
    //     return false
    // });

    //Added this to rook to update last active when browser is closed
    //--------------------------------
    const logout = () => {
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                // User is signed in.
                let userId = auth.currentUser.uid;
                userRef.child(userId).once('value').then(function (snapshot) {
                    let values = snapshot.val();
                    if (values) {
                        values["updatedAt"] = firebase.database.ServerValue.TIMESTAMP;
                        values["lastTerminate"] = firebase.database.ServerValue.TIMESTAMP;
                        userRef.child("test1").update(values);
                    }
                });
            } else {
                console.log('No user is signed in.');
            }
        });
    };
    window.addEventListener('unload', logout);
    window.addEventListener('offline', logout);
    //----------------------------------

    //Load previous messages in chat when scroll to top of limit
    document.getElementsByClassName("previous")[0].addEventListener("click", () => {
        let opsid = 0;
        messages.forEach(a => {
            if (opsid < limit) {
                //  messageHTML(messages[no]);
                no++;
                scrollTop();
                if (no >= messages.length) {
                    $(".message-previous").hide();
                }
            }
            opsid++;
        });

        $('.placeholder').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }

        });
        return false
    });


    //Get scroll to bottom element when at the top of the chat
    document.getElementById("scroll").addEventListener("click", () => {
        const cc = $('#conversation');
        const dd = cc[0].scrollHeight;
        cc.animate({
            scrollTop: dd
        }, 500);
        return false
    });

    //clicking on the home button at top
    //TODO: redudndant
    $('body').on('click', '.heading-home', function () {
        uKe = $(this).attr("id");
        uTipe = $(this).data("tipe");
        headingHTML($(this).data("avatar"), uKe, uTipe);
        inbox = 0;
        $(".inbox-status").hide();
        $('.side-one .sideBar-body').removeClass("active");
        chatMysql('rooms', 'Public', a => {
            messages = a;
            no = 0;
            document.getElementsByClassName('messages')[0].innerHTML = "";
            if (messages.length <= limit) {
                $(".message-previous").hide();
            } else {
                $(".message-previous").show();
            }
            let opsid = 0;
            messages.forEach(a => {
                if (opsid < limit) {
                    //     messageHTML(messages[no]);
                    no++;
                }
                opsid++;
            });

            $('.placeholder').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-img-mobile',
                image: {
                    verticalFit: true
                }

            });
            scrollBottom();
        });
        const $window = $(window);

        function checkWidth() {
            const windowsize = $window.width();
            if (windowsize <= 700) {
                document.getElementsByClassName("side")[0].style.display = "none";
            }
        }

        checkWidth();
        $(window).resize(checkWidth);
        return false
    });

    //Searching for chat in list
    $('body').on('keydown', '#searchText', () => {
        setTimeout(() => {
            if (document.getElementById("searchText").value == "") {
                $("body .side-one .sideBar-body").show();
            } else {
                $("body .side-one .sideBar-body").hide();
                $("body .side-one .sideBar-body").each((i, a) => {
                    const key = $("body .side-one .sideBar-body").eq(i).attr('id');
                    const reg = new RegExp(document.getElementById("searchText").value, 'ig');
                    const res = key.match(reg);
                    if (res) {
                        $("body .side-one .sideBar-body").eq(i).show();
                    }
                });
            }

        }, 50);
    });

    //Searching through contacts in list
    //TODO: redundant
    $('body').on('keydown', '#composeText', () => {
        setTimeout(() => {
            if (document.getElementById("composeText").value == "") {
                $("body .side-two .sideBar-body").show();
            } else {
                $("body .side-two .sideBar-body").hide();
                $("body .side-two .sideBar-body").each((i, a) => {
                    const key = $("body .side-two .sideBar-body").eq(i).attr('id');
                    const reg = new RegExp(document.getElementById("composeText").value, 'ig');
                    const res = key.match(reg);
                    if (res) {
                        $("body .side-two .sideBar-body").eq(i).show();
                    }
                });
            }

        }, 50);
    });


    //Tapping a temporarily added image:- removes it
    $('body').on('click', '.tmpImg', function () {
        const k = $(this).data('idx');
        tampungImg = tampungImg.filter(({name}) => name !== k);
        $(this).remove();
        if (tampungImg.length < 1) {
            document.getElementsByClassName('.imagetmp')[0].style.display = "none";
        }
        return false;
    });

    //Event handler for scrolling through the conversations, hides and what not
    $("body #conversation").scroll(function () {
        // scroll bottom
        if ($(this).scrollTop() >= ($("body .messages").height() - $(this).height())) {
            $("body .message-scroll").hide();
            $("body .message-previous").hide();
            return false;
        } else if ($(this).scrollTop() == 0) {
            if (no >= messages.length) {
                $("body .message-previous").hide();
            } else {
                $("body .message-previous").show();
            }
            return false;
        } else {
            $("body .message-previous").hide();
            $("body .message-scroll").show();
            return false;
        }
    });

    //Image button and what to do with it:- was replaced with image icon
    //TODO: find better way to handle collecting files
    document.getElementById('fileinput').addEventListener('change', readMultipleImg, false);
};
