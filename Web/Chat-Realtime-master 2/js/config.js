const domain = "http://localhost:8081";

// MySQL API
const apis = 'api.php';

// set image directory
const imageDir = 'image';

// const firebaseConfig = {
//     apiKey: "AIzaSyDrB9R8HxvNI3kSNfFRDIN_JcXooc9O62M",
//     authDomain: "startup-8a1bb.firebaseapp.com",
//     databaseURL: "https://startup-8a1bb.firebaseio.com",
//     projectId: "startup-8a1bb",
//     storageBucket: "startup-8a1bb.appspot.com",
//     messagingSenderId: "499845531798",
//     appId: "1:499845531798:web:de5c8a486247f952"
// };

const MainfirebaseConfig = {
    apiKey: "AIzaSyDORrkXt185UGpZqQgdLVmFF_awzsQs2mM",
    authDomain: "rookplus.firebaseapp.com",
    databaseURL: "https://rookplus.firebaseio.com",
    projectId: "rookplus",
    storageBucket: "rookplus.appspot.com",
    messagingSenderId: "710438680250",
    appId: "1:710438680250:web:2cdd2b787bf52d8e"
};

firebase.initializeApp(MainfirebaseConfig);

// create firebase child
const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJmaXJlYmFzZS1hZG1pbnNkay10MDU3eUByb29rcGx1cy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsInN1YiI6ImZpcmViYXNlLWFkbWluc2RrLXQwNTd5QHJvb2twbHVzLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiaHR0cHM6XC9cL2lkZW50aXR5dG9vbGtpdC5nb29nbGVhcGlzLmNvbVwvZ29vZ2xlLmlkZW50aXR5LmlkZW50aXR5dG9vbGtpdC52MS5JZGVudGl0eVRvb2xraXQiLCJ1aWQiOiJ0ZXN0MSIsImlhdCI6MTU2NzMyNDExMywiZXhwIjoxNTY3MzI3NzEzfQ.GQNL3fnUoHruSlEncp4rm0X8AWDa8as3o2VGr8_BJu_l1o_YBnmpMvgsLTqF_buNtAE-d5s6wLOURBh3ncVVKU9jubGWRx18dEEpjhpvpdCZ3-m04W36wpO_x_e1aeUOmLRf4VtjpKkM9WxSO3D21J8VMNFqIFJ_1aDnH1VCh0bQ-swWycsvnpwcMO0RkdpCwMQvIQeYmdobj2dR7Exzl5h2yb7jCDeniqAebcRNlAokn1nvKYPMWTdIFT5wKaNz3IYF44NHnCLvkl7Ib5HYc3K19Scg03pocoZPmkhF6w6-wfvXEzcjA52kWYqA_4D3MVKNZPrMaqrh4lfkW9y8CQ";
const auth = firebase.auth();
auth.signInWithCustomToken(token).catch(function (error) {
    // Handle Errors here.
    console.log(error.code);
    console.log(error.message);
    // ...
});
//console.log(auth.currentUser.uid);
const messageRef = firebase.database().ref('Message');
const userRef = firebase.database().ref('User');
const chatRef = firebase.database().ref('Chat');


const FCHAT_PATH = "Chat";					//	Path name
const FCHAT_OBJECTID = "objectId";			//	String
const FCHAT_CHATID = "chatId";			//	String

const FCHAT_MEMBERS = "members";			//	Array
const FCHAT_LINKEDS = "linkeds";			//	Dictionary

const FCHAT_SENDERID = "senderId";		//	String
const FCHAT_SENDERFULLNAME = "senderFullname";	//	String
const FCHAT_SENDERINITIALS = "senderInitials";	//	String
const FCHAT_SENDERPICTUREAT = "senderPictureAt";//	Timestamp

const FCHAT_RECIPIENTID = "recipientId";	//	String
const FCHAT_RECIPIENTFULLNAME = "recipientFullname";	//	String
const FCHAT_RECIPIENTINITIALS = "recipientInitials";	//	String
const FCHAT_RECIPIENTPICTUREAT = "recipientPictureAt";//	Timestamp

const FCHAT_GROUPID = "groupId";	//	String
const FCHAT_GROUPNAME = "groupName";		//	String

const FCHAT_LASTMESSAGETEXT = "lastMessageText";	//	String
const FCHAT_LASTMESSAGEDATE = "lastMessageDate";	//	Timestamp

const FCHAT_TYPINGS = "typings";			//	Dictionary
const FCHAT_LASTREADS = "lastReads";		//	Dictionary
const FCHAT_MUTEDUNTILS = "mutedUntils";//	Dictionary

const FCHAT_ARCHIVEDS = "archiveds";	//	Dictionary
const FCHAT_DELETEDS = "deleteds";	//	Dictionary

const FCHAT_CREATEDAT = "createdAt";	//	Timestamp
const FCHAT_UPDATEDAT = "updatedAt";	//	Timestamp

//---------------------------------------------------------------------------------
const FMESSAGE_PATH = "Message";				//	Path name
const FMESSAGE_OBJECTID = "objectId";				//	String

const FMESSAGE_CHATID = "chatId";				//	String
const FMESSAGE_MEMBERS = "members";				//	Array

const FMESSAGE_SENDERID = "senderId";		//	String
const FMESSAGE_SENDERFULLNAME = "senderFullname";	//	String
const FMESSAGE_SENDERINITIALS = "senderInitials";	//	String
const FMESSAGE_SENDERPICTUREAT = "senderPictureAt";//	Timestamp

const FMESSAGE_RECIPIENTID = "recipientId";//	String
const FMESSAGE_RECIPIENTFULLNAME = "recipientFullname";//	String
const FMESSAGE_RECIPIENTINITIALS = "recipientInitials";//	String
const FMESSAGE_RECIPIENTPICTUREAT = "recipientPictureAt";	//	Timestamp

const FMESSAGE_GROUPID = "groupId";		//	String
const FMESSAGE_GROUPNAME = "groupName";		//	String

const FMESSAGE_TYPE = "type";			//	String
const FMESSAGE_TEXT = "text";		//	String

const FMESSAGE_PICTUREWIDTH = "pictureWidth";	//	Number
const FMESSAGE_PICTUREHEIGHT = "pictureHeight";	//	Number
const FMESSAGE_VIDEODURATION = "videoDuration";	//	Number
const FMESSAGE_AUDIODURATION = "audioDuration";	//	Number

const FMESSAGE_LATITUDE = "latitude";	//	Number
const FMESSAGE_LONGITUDE = "longitude";	//	Number

const FMESSAGE_STATUS = "status";		//	String
const FMESSAGE_ISDELETED = "isDeleted";	//	Boolean

const FMESSAGE_CREATEDAT = "createdAt";		//	Timestamp
const FMESSAGE_UPDATEDAT = "updatedAt";		//	Timestamp
//---------------------------------------------------------------------------------
const POST_SIDEBAR_REFRESH = "refreshSidebar";
const POST_MESSAGE_REFRESH = "refreshMessages";

const decrypt = (text, password) => {
    let pass = sjcl.codec.hex.fromBits(sjcl.hash.sha1.hash(password));
    let dataEncrypted = sjcl.codec.base64.toBits(text);
    let bytes = RNCryptor.Decrypt(pass, dataEncrypted);
    return sjcl.codec.utf8String.fromBits(bytes);
};

const caseInsensitiveSort = (a, b) => {
    return a.toLowerCase().localeCompare(b.toLowerCase());
};

const descCompare = (a, b) => {
    // Use toUpperCase() to ignore character casing
    const A = a.lastMessageDate;
    const B = b.lastMessageDate;

    let comparison = 0;
    if (A > B) {
        comparison = 1;
    } else if (A < B) {
        comparison = -1;
    }
    return comparison;
};

let currentUser = "";